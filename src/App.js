import { UserProvider } from './UserContext'
import { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; 
import { Container } from 'react-bootstrap';

import './App.css'
import Home from './pages/Home'
import AppNavbar from './components/AppNavbar'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Menu from './pages/Menu'
import Products from './pages/Products'
import Admin from './adminPages/Admin'
import AddProduct from './adminPages/AddProduct'
import UpdateProduct from './adminPages/UpdateProduct';
import Archive from './adminPages/Archive';
import ErrorPage from './pages/ErrorPage';
import Cart from './pages/Cart'
import Checkout from './pages/Checkout';
import Order from './pages/Order'
import User from './pages/User'




function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()

    setUser({
      id: null,
      isAdmin: null
    });
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    
    <UserProvider value = {{user, setUser, unsetUser}}>
    
      <Router> 
     
        <AppNavbar />
        <Container Fluid>
        <Routes>
            <Route path="/" element={<Home />} />
           
            <Route exact path="*" element={<ErrorPage />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/register" element={<Register />} />
            <Route path="/checkEmail" element={<Register />} />
            <Route path="/products" element= {<Products/>} />
            <Route path="/menu" element={<Menu />} /> 
            <Route path="/cart" element={<Cart/>}/>
            <Route path="/myOrders" element={<Order />}/>
            <Route path="/checkout" element={<Checkout/>}/>
            <Route path="/create" element={<AddProduct />} /> 
            <Route path="/:productId/updateProduct" element= {<UpdateProduct/>} />
            <Route path="/:productId/archive" element= {<Archive/>} />

            <Route path="/user" element={<User />} /> 
            <Route path="/admin" element={<Admin />} />

            
           
        </Routes>
        </Container>
      </Router>
    </UserProvider>
 )
}

export default App;



