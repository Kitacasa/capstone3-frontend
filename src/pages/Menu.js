import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ActiveProducts() {

    const { user } = useContext(UserContext);

    const [ products, setProducts ] = useState([]);

    useEffect(() => {
        fetch('http://localhost:3000/products/searchActive')
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setProducts(data.map(product => {
                return (
                    <ProductCard key = {product._id} productProp ={product} />
                )
            }))
        })
    }, [])

    return (
        (user.isAdmin)?
            <Navigate to='/admin' />
        
            :

        <>
            <h1>MENU</h1>
            {products}
        </>
    )
}