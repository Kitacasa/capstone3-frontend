import { NavLink, Navigate } from 'react-router-dom'
import { Container, Row, Button } from 'react-bootstrap'


export default function Home() {

    return(
        <Container id='home-container' fluid style={{height: '100%', width:'80%', paddingTop: '10px'}}>
            {
                (!localStorage.getItem('token'))?
                <center><NavLink style ={{textDecoration: 'none', color: 'black', fontWeight: 'bold'}} to ='/register'>
                        <Button variant ='success' size='lg' className='w-50 mb-3 sign-up-btn'>Sign Up Now</Button>
                    </NavLink>
                </center>
            :
       <></> 
            }
           {/* (user.isAdmin)?
            <Navigate to = "/admin" />
            :*/}
            <Row className='home-banner' >
            <img className='img-home-banner' src='./images/BG_1.jpg' >
            </img>
            </Row>
       </Container>
    )
}
