import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { GiCupcake } from "react-icons/gi";
import { AiOutlineFieldNumber } from 'react-icons/ai'

export default function OrderPage() {

    const [ myOrders, setmyOrders ] = useState([]);
    const [ orderHistory, setOrderHistory ] = useState([]);

    useEffect(() => {
        fetch('http://localhost:3000/orders/orderHistory', {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            setOrderHistory(data)
        })
    }, [orderHistory])

    return(
        <>
            <div className='my-4'>
                <h1>Order History</h1>
            </div>

            <Table striped bordered hover responsive>
                <thead className='bg-dark text-warning'>
                    <tr>
                        <th>ORDER ID</th>
                        <th>PRODUCTS</th>
                        <th>TOTAL AMOUNT</th>
                        <th>DATE OF PURCHASE</th>
                    </tr>
                </thead>
                <tbody>
                    { orderHistory }
                </tbody>
            </Table>
        </>
    )
}