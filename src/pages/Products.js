import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';


export default function Products(){
    const [ products, setProducts ] = useState([]);

    useEffect(() => {
        fetch('http://localhost:3000/products/searchAll')
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} productProp = {product} />
                )
            }))
        })
    }, [])

    return (
        <>
            <h1>MENU</h1>
            {products}
        </>
    )
}

