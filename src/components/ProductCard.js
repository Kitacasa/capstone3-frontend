import { useContext } from "react";
import { Card, Button } from 'react-bootstrap';
import { Link, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from 'sweetalert2';



export default function MenuCard({ productProp }) {
    const { user } = useContext(UserContext);
    const { _id, name, description, price, stock, isActive } = productProp;

	const order = (productId, quantity, totalAmount) =>{
        console.log(productId, quantity, totalAmount);
        console.log(localStorage.getItem('token'));
		fetch('http://localhost:3000/orders/checkout', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
                quantity: quantity,
                totalAmount: totalAmount
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log("Order info: ", data);
			//console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully Ordered",
					icon: "success",
					text: "You have successfully placed an order."
				});

				Navigate("/menu");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	const cart = (productId, quantity, totalAmount) =>{
        console.log(productId, quantity, totalAmount);
        console.log(localStorage.getItem('token'));
		fetch('http://localhost:3000/orders/cart', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
                quantity: quantity,
                totalAmount: totalAmount
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log("Cart info: ", data);
			//console.log(data);

			if(data){
				Swal.fire({
					title: "Added to Cart",
					icon: "success",
					text: "You have successfully added the item(s)!"
				});

				Navigate("/menu");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}


    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                {(user.isAdmin) ? 
                    <>
                        <Card.Subtitle>Stock:</Card.Subtitle>
                        <Card.Text>{stock}</Card.Text>
                        <Card.Subtitle>Status:</Card.Subtitle>
                        <Card.Text>{isActive ? 'Active' : 'Inactive'}</Card.Text>
                        <Button className='m-2 p-2' as = {Link} to = {`/${_id}/updateProduct?name=${name}&description=${description}&price=${price}&stock=${stock}`}>
                            Edit
                        </Button>
                        {isActive ? (
                            <Button className='m-2 p-2' variant='danger' as = {Link} to = {`/${_id}/archive`}>
                                Archive
                            </Button>
                        )
                        :
                        (
                            <Button className='m-2 p-2' as = {Link} to = {`/${_id}/archive`}>
                                Unarchive
                            </Button>
                        )}
                    </>
                 
                :
                
                    <>
                    <Card.Subtitle>Quantity:</Card.Subtitle>
                    <Card.Text>1 pc</Card.Text>
                    <Button className='m-2 p-2' variant = 'primary' onClick={() => cart(_id, 1, price)}>
                        Add Cart
                    </Button>
                    <Button className='m-2 p-2' variant = 'primary' onClick={() => order(_id, 1, price)}>
                        Order Now
                    </Button>
                    </>
                }
            </Card.Body>
        </Card>
    )
}