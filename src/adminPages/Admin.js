import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Table, Button } from 'react-bootstrap';
import AddProduct from './AddProduct';
import { GiCupcake } from "react-icons/gi";
import { AiOutlineFieldNumber } from "react-icons/ai";


export default function AdminView(props) {

    const { productsData, fetchData } = props;
    const navigate = useNavigate();
    const [ products, setProducts ] = useState([])
    


    useEffect(() => {

        const productsArr = (product => {
            return(
                <tr key={product._id}>
                    <td colSpan="2" className="bg-light text-success" style={{fontWeight: 'bold'}}><AiOutlineFieldNumber /> {product._id}</td>
                    {/*<td>
                        <img 
                        src={product.image}
                        width="200px"
                        />
                    </td>*/}
                    <td className="bg-light text-dark"><GiCupcake /> {product.name}</td>
                    <td>{product.description}</td>
                    <td className="bg-light text-danger" style={{fontWeight: 'bold'}}><span>&#8369;</span>{product.price}</td>
                    <td className={
                        product.isActive ? 
                            "text-success bg-light" : "text-danger bg-light" 
                        }>
                        {
                            product.isActive ? "Available" : "Unavailable"}
                    </td>
                    
            
                    
                </tr>
                )
        })
        setProducts(productsArr)
    }, [productsData])

    const getAllProducts =() =>{
        navigate('/products')
    }


    return(
        <>
            <div className="mt-5 mb-3 text-center">
                <h1>Admin Dashboard</h1>
                <AddProduct fetchData={fetchData} />
                <Button className='bg-warning text-dark mx-4' variant='dark' size='lg' style={{fontWeight: 'bold'}} onClick={getAllProducts}>All Products</Button>
            </div>
            
            
            <Table striped bordered hover responsive>
                <thead className="bg-dark text-warning">
                    <tr>
                        <th colSpan="2">ID</th>
                        {/*<th>IMAGE</th>*/}
                        <th>NAME</th>
                        <th>DESCRIPTION</th>
                        <th>PRICE</th>
                        <th>AVAILABILITY</th>
                        <th colSpan="2">ACTIONS</th>
                    </tr>
                </thead>

                <tbody>
                    {products}
                </tbody>
            </Table>

        </>

        )
}

