import { useState, useEffect, useContext } from 'react';
import { Form, Button, Modal, ModalFooter } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useLocation } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function UpdateProduct({fetchData}) {

    const {user} = useContext(UserContext);
    const { productId } = useParams();
    const { search } = useLocation();

    const queryParams = new URLSearchParams(search);
    const queryName = queryParams.get('name');
    const queryDesc = queryParams.get('description');
    const queryPrice = queryParams.get('price');
    const queryStock = queryParams.get('stock');

    
   // const [productId, setProductId] = useState('');
   const [name, setName] = useState(queryName);
   const [description, setDescription] = useState(queryDesc);
   const [price, setPrice] = useState(queryPrice);
    const [stock, setStock] = useState(queryStock);
    const [isActive, setIsActive] = useState(false);

    const [ showEdit, setShowEdit ] = useState(false);
    const openEdit = () => setShowEdit(true);
    const closeEdit = () => setShowEdit(false);
    

    function updateProduct(e) {
        e.preventDefault();


        fetch(`http://localhost:3000/products/${productId}/update`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                stock: stock         
            })

            
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title:'Success',
                    icon: 'success',
                    text: 'Product Successfully Updated!'
                })
            
            } else {
                Swal.fire({
                    title: 'error',
                    icon: 'error',
                    text: 'Something went wrong!'
                })
                
            }

            setName('');
            setDescription('');
            setPrice('');
            setStock('');
        })
    }

    useEffect(() => {
        if((name !== '' || description !== '' || price !== '' || stock !== '')) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [productId, name, description, price, stock])



    return (

        <>
            <Button className='bg-warning text-dark' variant='dark' size='lg' style={{fontWeight: 'bold'}} onClick={openEdit}>Update</Button>

            <Modal show={showEdit} onHide={closeEdit}>

            <Form onSubmit={(e) => updateProduct(e)}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Product</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group controlId = "productName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                        type="text"
                        placeholder={queryName}
                        //required
                        value={name}
                        onChange={e => setName(e.target.value)}
                        />
                    </Form.Group>

            <Form.Group controlId = "productDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    placeholder={queryDesc}
                    //required
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                />
            </Form.Group>

            <Form.Group controlId = "productPrice">
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="text"
                    placeholder={queryPrice}
                    //required
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                />
            </Form.Group>

            <Form.Group controlId = "productStock">
                <Form.Label>Stock</Form.Label>
                <Form.Control
                    type="number"
                    placeholder={queryStock}
                    //required
                    value={stock}
                    onChange={e => setStock(e.target.value)}
                />
            </Form.Group>
            </Modal.Body>

            
            <Modal.Footer>
                    <Button variant="dark" onClick={closeEdit}>Close</Button>
                    <Button variant="warning" type="submit">Submit</Button>
            </Modal.Footer>

        </Form>
        </Modal>
        </>
    )
}
