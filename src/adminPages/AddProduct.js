import { useState, useEffect, useContext } from 'react';
import { Form, Button, Modal, ModalFooter } from 'react-bootstrap';
//import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Create() {
    const {user} = useContext(UserContext);

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [stock, setStock] = useState('');
    const [isActive, setIsActive] = useState(false);

    const [ showAdd, setShowAdd ] = useState(false);

    const openAdd = () => setShowAdd(true);
    const closeAdd = () => setShowAdd(false);


    function createProduct(e) {
        e.preventDefault();

        
        fetch('http://localhost:3000/products/create', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                stock: stock              
            })

            
        })
        .then(res => res.json())
        .then(data => {
            
            if(data){
                Swal.fire({
                    title:'Success',
                    icon: 'success',
                    text: 'Product Successfully Added'
                })

                // CLose modal
                closeAdd()

              
            } else {
                Swal.fire({
                    title: 'error',
                    icon: 'error',
                    text: 'Something went wrong!'
                })

    
            }

            setName('');
            setDescription('');
            setPrice('');
            setStock('');
        })
    }


    return (

        <>
            <Button className='bg-warning text-dark' variant='dark' size='lg' style={{fontWeight: 'bold'}} onClick={openAdd}>Add New Product</Button>

            <Modal show={showAdd} onHide={closeAdd}>

            <Form onSubmit={(e) => createProduct(e)}>
                <Modal.Header closeButton>
                    <Modal.Title>Add Product</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group controlId = "productName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                        type="text"
                        placeholder="Enter Product Name"
                        required
                        value={name}
                        onChange={e => setName(e.target.value)}
                        />
                    </Form.Group>

            <Form.Group controlId = "productDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter Product Description"
                    required
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                />
            </Form.Group>

            <Form.Group controlId = "productPrice">
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter Product Price"
                    required
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                />
            </Form.Group>

            <Form.Group controlId = "productStock">
                <Form.Label>Stock</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Enter Stock"
                    required
                    value={stock}
                    onChange={e => setStock(e.target.value)}
                />
            </Form.Group>
            </Modal.Body>

            
            <Modal.Footer>
                    <Button variant="dark" onClick={closeAdd}>Close</Button>
                    <Button variant="warning" type="submit">Submit</Button>
            </Modal.Footer>

        </Form>
        </Modal>
        </>
    )
}
